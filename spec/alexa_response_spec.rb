# rspec spec/alexa_response_spec.rb
require 'alexa/response'

RSpec.describe Alexa::Response do
    describe '.build' do
        it 'returns a JSON response with a custom string if provided' do
          expected_response = {
            version: "1.0",
            response: {
              outputSpeech: {
                type: "PlainText",
                text: "Custom String"
              }
            }
          }.to_json
    
          expect(Alexa::Response.build("Custom String")).to eq expected_response
        end

        it 'returns a minimal JSON response otherwise' do
            # this is where our previous test goes
            # as we still want to be able to call
            # Alexa::Response.build (with no parameters)
            # and have that return the minimal response
            minimal_response = {
                version: "1.0",
                response: {
                  outputSpeech: {
                    type: "PlainText",
                    text: "Really? I can only tell you about numbers. My favorite number is one, because it is the loneliest number that you will ever do."
                  }
                }
              }.to_json
        
              expect(Alexa::Response.build).to eq minimal_response
          end
    end
end