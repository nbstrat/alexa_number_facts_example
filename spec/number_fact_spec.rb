# rspec spec/number_fact_spec.rb --format documentation

require 'number_fact'

RSpec.describe NumberFact do
  describe '#text' do
    it 'returns a fact for a given number and fact type as plain text' do
      number_fact_text = "3 is the number of spatial dimensions we perceive our universe to have."
      client = double("Net::HTTP", get: number_fact_text)
      number_fact = described_class.new("3", "trivia", client)

      expect(number_fact.text).to eq number_fact_text
    end
  end
end


RSpec.describe NumberFact do
  describe '#initialize' do
    it 'a valid fact_type parameter returns unchanged' do
      number_fact_text = "3 is the number of spatial dimensions we perceive our universe to have."
      client = double("Net::HTTP", get: number_fact_text)
      number_fact = described_class.new("3", "math", client)

      expect(number_fact.fact_type).to eq "math"
    end
  #end

    it 'an invalid fact_type parameter will default to trivia' do
      number_fact_text = "3 is the number of spatial dimensions we perceive our universe to have."
      client = double("Net::HTTP", get: number_fact_text)
      number_fact = described_class.new("3", "bicycle", client)

      expect(number_fact.fact_type).to eq "trivia"
    end

    it 'an nil fact_type parameter will default to trivia' do
      number_fact_text = "3 is the number of spatial dimensions we perceive our universe to have."
      client = double("Net::HTTP", get: number_fact_text)
      number_fact = described_class.new("3", nil, client)

      expect(number_fact.fact_type).to eq "trivia"
    end
  end
end