# rspec spec/alexa_request_spec.rb --format documentation
require 'alexa/request'

RSpec.describe Alexa::Request do
    describe '#slot_value' do
        it 'returns the value for a specified slot' do
            request_json = {
                "request": {
                    "type": "IntentRequest",
                    "intent": {
                    "name": "IntentName",
                    "slots": {
                        "SlotName": {
                        "name": "SlotName",
                        "value": "10"
                        }
                    }
                    }
                }
            }.to_json
    
        # Now we must mock the behaviour of the
        # incoming Sinatra request, with a #body
        # method that yields a StringIO containing
        # the JSON we are ultimately dealing with
        sinatra_request = double("Sinatra::Request", body: StringIO.new(request_json))

        expect(Alexa::Request.new(sinatra_request).slot_value("SlotName")).to eq "10"
      end
    end
end

RSpec.describe Alexa::Request do
        describe '#intent_name' do
          it 'returns the name of the requested intent' do
            request_json = {
                "request": {
                    "type": "IntentRequest",
                    "intent": {
                    "name": "IntentName",
                    "slots": {
                        "SlotName": {
                        "name": "SlotName",
                        "value": "10"
                        }
                    }
                    }
                }
            }.to_json
    
          sinatra_request = double("Sinatra::Request", body: StringIO.new(request_json))

          expect(Alexa::Request.new(sinatra_request).intent_name()).to eq "IntentName"
       end
    end
end