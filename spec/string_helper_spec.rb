# rspec spec/string_helper_spec.rb --format documentation
require 'helpers/string_helper'

RSpec.describe StringHelper do
  describe 'is_number(5)' do
    it 'returns true when string represents a numerical value' do
      input = "5"
      expect(StringHelper.is_number?(input)).to eq true
    end
  end

  describe 'is_number(five)' do
    it 'returns false when string represents a number spelled out' do
      input = "five"
      expect(StringHelper.is_number?(input)).to eq false
    end
  end

  describe 'is_number(non number text)' do
    it 'returns false when string does not represent a number' do
      input = "not a number"
      expect(StringHelper.is_number?(input)).to eq false
    end
  end
end