# Alexa Number Facts Tutorial
https://developer.amazon.com/blogs/post/50f1c350-26ae-4032-b4a3-156c5f35c448/makers-academy-s-alexa-series-build-a-fact-checking-skill-with-slots-and-custom-slot-types

https://developer.amazon.com/alexa-skills-kit/makers-academy

# Using Command Prompt with Ruby for all terminal operations
1) Create the project directory (alexa-number-facts)
2) Head into the directory with cd c:\_projects\alexa-number-facts
3) Run the following in the terminal:

bundle init

4) Modify the gemfile created with bundle init to include sinatra
gem 'sinatra'

5) Execute bundle install to install Sinatra

bundle install

6) Create a server file (server.rb) with a simple POST route and response. 

Log into Amazon Developer Console (ADC https://developer.amazon.com) and ...
Create a new skill, add a simple utterance, save and build the model. 

7) Start up ngrok and take note of the https forwarding address.
8) ADC - configure the HTTPS endpoint with the forwarding address from ngrok and select 
"My development endpoint is a sub-domain of domain that has a wildcard certifiate." for the SSL certificate type.

Start the local Ruby server 
ruby server.rb

Use the Alexa Simulator in ADC to verify that everthing is configured propertly.
Number Facts about forty two
number fact about seven
NumberFact about forty two
NOTE TO ME: Alexa Simulator responds as expected with the skill name, but when entering the utterances I do not get the expected results. 
e.g.
Invocation Name: "Number Facts"
Utterance: "number fact about seven"
In the simulator, entering "Number Facts" or any case variation of the phrase will return the response from my local endpoint.
entering the skill name and an utterance will return Hmmm, I don't know that one.
Including the work "Ask" seems to resolve this, but do not really see why.
"Ask <InvocationName> <Utterance>"
"Ask number facts about five"
Including additional verbiage such as "Ask number facts about the number five" would seem to be working as expected 
BUT using "Ask number fact" followed by any random text will retur the same response since this is such a simple test case.


After confirming that the simple configuruation is working correctly, time to get the debugging environment configured.
gem install ruby-debug-ide
gem install debase

Add the VSCode configuration file and modify the port value to use the port being used by ngrok (4567) 

To Debug, start the debug listener from the terminal
rdebug-ide --host 0.0.0.0 --port 4567 --dispatcher-port 26162 server.rb

The terminal will respond with 
listens on 0.0.0.0:4567

Select Listen for rdebug-ide from the VSCode debug selection. The terminal will respond with the following after successfully
attaching to the debug process and the VSCode status bar will change from blue to orange indicating that it is in debug mode.

== Sinatra (v2.0.5) has taken the stage on 4567 for development with backup from Puma
*** SIGUSR2 not implemented, signal based restart unavailable!
*** SIGUSR1 not implemented, signal based restart unavailable!
*** SIGHUP not implemented, signal based logs reopening unavailable!
Puma starting in single mode...
* Version 3.12.0 (ruby 2.5.3-p105), codename: Llamas in Pajamas
* Min threads: 0, max threads: 16
* Environment: development
* Listening on tcp://localhost:4567
---------------------------------------------------------------------

Added slots to the NumberFactsIntent and a custom slot type to handle the "trivia" and  "math" facttypes.
ask Number Facts to tell me a math fact about 17
ask Number Facts to tell me a trivia fact about 5

ask Number Facts to tell me a {FactType} fact about {number}

ask number facts trivia about 5
ask number facts math fact about the number 5
ask number facts a math fact about the number 6
ask number facts a trivia fact about the number 6
ask number facts a math fact about the number 4
ask number facts for help


# Unit Testing
# --------------------------------------
### Reference: https://developer.amazon.com/blogs/alexa/post/35bdad3d-57c8-4623-88c6-815540697af5/unit-testing-create-functional-alexa-skills

#### Install frameworks via npm
At the root level of the project, execute the following from a terminal:
npm install ask-sdk-core
npm install ask-sdk-model

Create a test folder and cd to that location. Execure the following from a terminal:
npm install alexa-skill-test-framework --save-dev
npm install mocha --global

I was running an older version of Node.js (v0.12.2) and updated to the latest (v10.15.0) to overcome issues with the const keywork used within mocha.

Summary: (in testing branch)
These unit tests are testing the lambda-custom/index.js file - not the actual ruby code that needs to be tested.
In short, this is the direction to go IF you are writing the code in javascript, but not ideal for testing 
ruby based Skills.

TODO: Research RSpec, UNIT::TEST, Cucumber so the ruby/sinatra application can have
adequate unit testing. 
-------------------------------------------

RSpec:
Referenced this tutorial:
https://developer.amazon.com/blogs/alexa/post/001e7537-6702-4b58-b0d7-cbaed3a9ab3c/makers-academy-s-alexa-series-how-to-model-alexa-using-object-oriented-ruby

Ruby Style Guide:
https://github.com/rubocop-hq/ruby-style-guide