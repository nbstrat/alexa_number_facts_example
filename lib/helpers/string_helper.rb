# helpers/string_helper.rb
module StringHelper
  extend self

  def is_number? string
    string.to_f.to_s == string.to_s || string.to_i.to_s == string.to_s
  end
  
end