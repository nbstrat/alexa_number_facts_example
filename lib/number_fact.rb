# in lib/number_fact.rb
# maing a call to an external API will require Ruby's Net::HTTP library
require 'net/http'

class NumberFact
  attr_reader :text, :fact_type

  # we need to inject our double client to mock the HTTP call
  def initialize(number, fact_type, client = Net::HTTP)
    @fact_type = check_fact_type(fact_type)
    number_facts_uri = URI("http://numbersapi.com/#{ number }/#{ fact_type }")
    @text = client.get(number_facts_uri)
  end

  def check_fact_type(type)
    # default to trivia fact_type when invoked using a non-valid value
    fact_type = (type != "trivia") && (type != "math") ? "trivia" : type
  end

end