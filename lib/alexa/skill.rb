require_relative './handlers'

module Alexa
  class Skill
    # find all intent declarations in the /intents directory
    def self.register_intents
      Dir.glob("intents/*.rb").each { |intent_declaration| register(intent_declaration) }
    end

    # for each declaration, evaluate the declaration
    # in the context of the Alexa::Handlers class
    def self.register(intent_declaration)
      Alexa::Handlers.class_eval File.open(File.expand_path(intent_declaration)).read
    end
  end
end

# register any available intents
Alexa::Skill.register_intents