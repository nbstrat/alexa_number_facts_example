require './lib/alexa/request'
require './lib/alexa/response'

module Alexa
  class Handlers
    # Use a class variable to store a map of IntentNames and Intents
    @@intents = {}

    def initialize(request)
      @request = request
    end

    # Handle the incoming request. The request is expected to respond to #intent_name,
    # i.e. we are assuming the request is an instance of Alexa::Request.
    # we execute the intent registered to the request's IntentName within the context of
    # this Handlers instance: that way we have access to a bunch of convenience methods
    # to make the user's experience of writing intents more pleasant.
    def handle
      instance_eval &registered_intent(request.intent_name)
    end

    class << self
      # provide developers with a friendly interface for
      # defining custom intents, of the form 
      # `intent "IntentName" {}`
      # Store any intents written in this form inside the
      # class variable @@intents, for handling later
      def intent(intent_name, &block)
        @@intents[intent_name] = block
      end

      # a builder method that wraps the incoming request into an
      # Alexa::Request instance we can work with more easily
      def handle(request)
        new(Alexa::Request.new(request)).handle
      end
    end

    # allow developers to work directly with
    # requests in their intent declarations
    attr_reader :request

    def registered_intent(intent_name)
      @@intents[intent_name]
    end

    def respond(response_details)
      Alexa::Response.build(response_details)
    end

    private :request, :registered_intent
  end
end