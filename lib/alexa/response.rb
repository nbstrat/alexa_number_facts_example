require 'json'

module Alexa
  class Response < Hash
    
    def initialize(response_text)
        # in the initializer, build the response procedurally
        self[:version] = "1.0"
        self[:response] = Hash.new
        self[:response][:outputSpeech] = Hash.new
        self[:response][:outputSpeech][:type] = "PlainText"
        self[:response][:outputSpeech][:text] = response_text
    end
    
    
    # define a default value for the build
    def self.build(response_text = "Really? I can only tell you about numbers. My favorite number is one, because it is the loneliest number that you will ever do.")
       # in the builder, convert the response from the initializer to JSON
      new(response_text).to_json
    end
  end
end