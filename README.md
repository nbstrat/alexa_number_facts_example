## Alexa Skill - Number Facts Example

This Alexa Skills project is based on the Alexa Skills [tutorial](https://developer.amazon.com/blogs/post/50f1c350-26ae-4032-b4a3-156c5f35c448/makers-academy-s-alexa-series-build-a-fact-checking-skill-with-slots-and-custom-slot-types) provided by Makers Academy. This was my 2018 Christmas break project since things at work slow down enough that I have enough brain power left at the end of the day to apply to learning something new. 

Since I have very little Ruby development experience, these were my goals when starting this project:
* Increase my Ruby development experience.
* Learn how to create an Alexa Skill.
* Evaluate the VSCode IDE editor. 
* Look like a rock star when I request my Alexa Skill from other Echo devices as I visit friends and family during the holidays. 

 After completing the initial steps of the tutorial, configuring my environment to utilize ngrok, I added some additional functionality to check for missing slot values, etc but was not really happy with the structure of the code. What I had created was [Spaghetti Code](https://en.wikipedia.org/wiki/Spaghetti_code), did not contain any unit tests, and was not work that I was really proud of. I am a rock star, not a one hit wonder, so I did what you have to do to be a rock star - **work a little smarter**.

 ##### Eliminating Spaghetti Code
 What I was attempting to do was apply a modular structure from a MVC design pattern perspective to this project and eventually stumbled upon this additional [tutoria](https://developer.amazon.com/blogs/alexa/post/001e7537-6702-4b58-b0d7-cbaed3a9ab3c/makers-academy-s-alexa-series-how-to-model-alexa-using-object-oriented-ruby). This tutorial also included unit testing information and examples. I only wish I had found it earlier so I wouldn't have had to pester my bud Jim with so many Ruby based unit testing questions.

 During this coding phase, I eliminated nearly all of the spaghetti code, implemented unit test, and applied a modular structure. Having unit tests actually sped up my development efforts since testing Alexa Skills using the 
 [Amazon Developer Console](https://developer.amazon.com/home.html) and [Echosim](https://echosim.io) can become very tedious.  

 #### Structure
 ```bash
alexa-number-facts
├───.vscode (VS Code settings)
├───intents (Each intent should be defined as a seperate file.)
├───lib
│   ├───alexa 
│   └───helpers 
├───models (json output from Amazon Developer Console defining the Alexa Skill)
└───spec (RSpec unit tests)
```

 #### Quick Reference
  * Start ngrok
 ```
 ngrok http 4567
 ```
 Take note of the Fowarding address displayed in the ngrok terminal window. You MUST update your endpoint in the Amazon Developer Console to the https address to route Alexa requests to your local environment. 

  * Start the local ruby server
 ``` 
 ruby server.rb
 ```

* Start ruby server in debug mode
```
rdebug-ide --host 0.0.0.0 --port 4567 --dispatcher-port 26162 server.rb
```

### Sample Alexa Skill Utterances
* ask number facts trivia about 5
* ask number facts math fact about the number 5
* ask number facts a math fact about the number 6
* ask number facts a trivia fact about the number 6
* ask number facts a math fact about the number 4
* ask number facts for help


#### VSCode IDE
Key Features
* Has a terminal window allowing for multiple terminal to be open from the IDE. This is a huge time saver for me since I can have a bash shell terminal and command prompt terminal directly within the IDE!
* Debugging aspects very similiar to debugging in Visual Studio.
* Notification icons indicating which files have not been committed, etc, that is very similiar to other git based graphical interfaces such as SourceTree. This is better since I don't need to execute additional git commands or try to locate my bash shell terminal hidden behind other windows.
* VS Code is platform independant. I have not installed on a Linux box yet, but will be doing this very soon with the hope that my development environment can be somewhat consistent as I jump back and forth on different machines. 

Disappointments
* Have yet to find an easy way to maintain the VSCode settings.json file as part of my [dotfiles](https://github.com/nbstrat/dotfiles) respoitory that will work across multiple environments. 

##### Configuring Debugging with VS Code
https://dev.to/dnamsons/ruby-debugging-in-vscode-3bkj

When starting the server in debug mode, the terminal will respond with 
```listens on 0.0.0.0:4567```

Select 'Listen for rdebug-ide' from the VSCode debug selection. The terminal will respond with the following after successfully attaching to the debug process. The VSCode status bar will also change from blue to orange indicating that it is in debug mode.

```
== Sinatra (v2.0.5) has taken the stage on 4567 for development with backup from Puma
*** SIGUSR2 not implemented, signal based restart unavailable!

*** SIGUSR1 not implemented, signal based restart unavailable!
*** SIGHUP not implemented, signal based logs reopening unavailable!
Puma starting in single mode...
* Version 3.12.0 (ruby 2.5.3-p105), codename: Llamas in Pajamas
* Min threads: 0, max threads: 16
* Environment: development
* Listening on tcp://localhost:4567
```


##### AWS Cloud9 Development Environment
* Create Cloud9 environment
* Clone repo from GitHub
* Open a terminal and confirm ruby and gem installed
```
ruby --version
gem --version
```
* Run bundle to install the additional gems (aka Sinatra, rspec) as defined in the gem file.
```
bundle install
```

### TODO
* Move Ruby code to the Amazon platform
* Create an Alexa Skills bootstrap template based on the structure of this project.