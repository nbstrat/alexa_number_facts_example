
# Response for AMAZON.HelpIntent

intent "AMAZON.HelpIntent" do
 respond("I can provide interesting math and trivia facts about numbers. 
    Ask me using a phrase such as,  
    Ask Number Facts trivia about the number 5.")
end