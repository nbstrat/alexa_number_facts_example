# Response for NumberFactsIntent request 

require './lib/number_fact'
require './lib/helpers/string_helper'
require './lib/alexa/request'

intent "NumberFactsIntent" do
  
  number = request.slot_value("Number")
  fact_type = request.slot_value("FactType")

  if StringHelper.is_number?(number)
    response = NumberFact.new(number, fact_type).text
  else
    response = "Really? I can only tell you about numbers. My favorite number is one, because it is the loneliest number that you will ever do."
  end
  
  respond(response)
end